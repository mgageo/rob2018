# rob2018 : rencontres ornithologiques bretonnes 2018

Scripts en environnement Windows 10 : MinGW R MikTex

Ces scripts exploitent des données en provenance des bases Biolovision.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier ROB2018.
