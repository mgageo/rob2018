#!/bin/sh
# <!-- coding: utf-8 -->
#T rob2018 : atlas ornitho
# auteur: Marc Gauthier
[ -f ../win32/scripts/misc.sh ] && . ../win32/scripts/misc.sh
[ -f ../win32/scripts/mga.sh ] && . ../win32/scripts/mga.sh
#f CONF:
CONF() {
  LOG "CONF debut"
  CFG="ROB2018"
  [ -d "${CFG}" ] || mkdir "${CFG}"
  LOG "CONF fin"
}
#F GIT: pour mettre à jour le dépot git
GIT() {
  LOG "GIT debut"
  Local="${DRIVE}/web/geo";  Depot=rob2018; Remote=frama
  export Local
  export Depot
  export Remote
  _git_lst
  bash ../win32/scripts/git.sh INIT $*
#  bash ../win32/scripts/git.sh PUSH
  LOG "GIT fin"
}
#f _git_lst: la liste des fichiers pour le dépot
_git_lst() {
  cat  <<'EOF' > /tmp/git.lst
scripts/rob2018.sh
EOF
#  ls -1 scripts/rob2018*.R >> /tmp/git.lst
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/rob2018.R')" )
#  exit
  ls -1 ${CFG}/*.tex >> /tmp/git.lst
  cat  <<'EOF' > /tmp/README.md
# rob2018 : rencontres ornithologiques bretonnes 2018

Scripts en environnement Windows 10 : MinGW R MikTex

Ces scripts exploitent des données en provenance des bases Biolovision.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier ROB2018.
EOF
}
[ $# -eq 0 ] && ( HELP )
CONF
while [ "$1" != "" ]; do
  case $1 in
    -c | --conf )
      shift
      Conf=$1
      ;;
    * )
      $*
      exit 1
  esac
  shift
done