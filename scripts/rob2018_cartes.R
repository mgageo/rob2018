# <!-- coding: utf-8 -->
#
# quelques fonction pour les rob2018
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# la production des cartes
cartes_jour <- function() {
  carp()
}
# source("geo/scripts/rob2018.R");ign_dl()
# source("geo/scripts/rob2018.R");nc <- ign_ade_lire_sf('DEPARTEMENT')
# source("geo/scripts/rob2018.R");cartes_especes()
cartes_especes <- function(force=FALSE) {
  library(sf)
  cartes_fond_ign()
  dsn <- sprintf("%s/donnees.Rda", exportDir);
  load(file=dsn)
  print(colnames(rda.df))
  cols.num <- c("COORD_LON", "COORD_LAT", "ATLAS_CODE")
  rda.df[cols.num] <- sapply(rda.df[cols.num], as.numeric)
  rda.sf <- st_as_sf(rda.df, coords = c("COORD_LON", "COORD_LAT"), crs = 4326)
  rda.sf <- st_transform(rda.sf, "+init=epsg:2154")
  nc <- rda.sf %>%
    filter(!is.na(ATLAS_CODE))
  plot(st_geometry(nc), add=TRUE)
}
# source("geo/scripts/rob2018.R");cartes_fond_ign()
cartes_fond_ign <- function() {
  library(sf)
  library(tidyverse)
  nc <- ign_ade_lire_sf('DEPARTEMENT')
  departements <- c('22', '29', '35', '56')
  departements.sf <- filter(nc, INSEE_DEP %in% departements)
  opar <- par(mar = c(0,0,0,0))
  plot(st_geometry(departements.sf), bg='lightblue', col='white')
}