# <!-- coding: utf-8 -->
#
# quelques fonctions pour générer les fichiers tex
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
#
# la partie pour tex
tex_les <- function(df, suffixe="", dossier="images") {
  require(cowplot)
  df[is.na(df)] <- ""
  for(i in 1:nrow(df) ) {
    fonction <- df$fonction[i]
    if ( grepl('^#', fonction) ) {
      next
    }
    if ( grepl('^!', fonction) ) {
      break
    }
    tex_fonction(fonction, df$mode[i], df$args[i], suffixe, dossier)
  }
}
tex_fonction <-function(e, mode="carte", args = "", suffixe="", dossier="images") {
  library(stringr)
  print(sprintf("tex_fonction() e: %s mode: %s args: %s", e, mode, args))
  outputFic <<- sprintf("%s/%s/%s.pdf", texDir, dossier, e)
  if ( suffixe != "" ) {
    outputFic <<- sprintf("%s/%s/%s%s.pdf", texDir, dossier, e, suffixe)
    a <- list(args)
  }
  fonction <- sprintf("%s_tex", e)
  if( ! exists(fonction, mode = "function") ) {
    fonction <- e
  }
  a <- list()
  if ( args != "" ) {
    outputFic <<- sprintf("%s\\%s\\%s_%s%s.pdf", texDir, dossier, e, args, suffixe)
    a <- list(args)
  }
  w <- 4
  h <- 4
#  mode <- "gg(4,8)"
  r <- "^(\\w+)\\((\\d+),(\\d+)\\)"
  m <- str_match_all(mode, r)
  m <- m[[1]]
#  print(class(m))
  if (nrow(m) > 0) {
    mode <- m[1,2]
    w <- m[1,3]
    h <- m[1,4]
  }

  if ( interactive() ) {
    switch(mode,
      "carte" = {
        dev.new(width = w, height = h, units = "px")
      },
      "ggplot" = {
        pdf(outputFic, width = w, height = h)
      },
      "plotgg" = {
        dev.new(width = w, height = h)
      },
      "print" = {
        dev.new()
      },
      {
        print(sprintf("tex_fonction() **** %s", mode))
      }
    )
  } else {
    switch(mode,
      "carte" = {
        outputFic <<- sub("pdf$", "png", outputFic)
        png(outputFic, width = ncols, height = nrows)
      },
      "ggplot" = {
        pdf(outputFic, width = w, height = h)
      },
      "plotgg" = {
        pdf(outputFic, width = w, height = h)
      },
      "print" = {
        pdf(outputFic)
      },
      {
        print(sprintf("tex_fonction() ****"))
      }
    )
  }
#  dev.new()
  if ( mode != "print" ) {
    par(mar=c(0,0,0,0), oma=c(0,0,0,0))
  }

  p <- do.call(fonction, a)
  if ( interactive() ) {
    if ( grepl("carte", mode) ) {
      savePlot(filename=outputFic,type=c("pdf"), device=dev.cur(), restoreConsole = TRUE)
    }
    if ( grepl("print", mode) ) {
      dev.print(pdf, outputFic)
    }
    if ( grepl("ggplot", mode) ) {
      save_plot(outputFic, p)
    }
    if ( grepl("plotgg", mode) ) {
      savePlot(filename=outputFic,type=c("pdf"), device=dev.cur(), restoreConsole = TRUE)
    }
  } else {
    dev.off()
  }
  print(sprintf("tex_fonction() fin e:%s outputFic: %s", e, outputFic))
}
#
# conversion en table latex
# http://math.furman.edu/~dcs/courses/math47/R/library/xtable/html/print.xtable.html
# pas en utf-8
tex_df2table <- function(df, dsn=FALSE, suffixe="", dossier="", num=FALSE, nb_lignes=50) {
  library(xtable)
  options("encoding" = "UTF-8")
  if ( dsn == FALSE ) {
    curcall <- as.character(deparse(sys.call(-1)))
    curcall <- gsub('\\(.*$', '', curcall)
    if ( suffixe != "" ) {
      suffixe <- sprintf("_%s", suffixe)
    }
    if ( dossier != "" ) {
      dossier <- sprintf("%s/", dossier)
    }
    dsn <- sprintf("%s/%s%s%s.tex", texDir, dossier, curcall, suffixe)
  }
#  View(df)
  df <- as.data.frame(df)
  ajout <- FALSE
  ligne <- 1
#  nb_lignes <- 50
  row.names(df) <- 1:nrow(df)
  while ( ligne < nrow(df) ) {
    fin <- ligne + nb_lignes - 1
    fin <- min(fin, nrow(df))
    dfl <- df[ligne:fin,]
#    Log(sprintf("atlas_combine_stat() ligne: %d nrow: %d", ligne, nrow(dfl)))
    df.table <- xtable(dfl, digits=0)
#    align(df.table, "rlrrrrr")
    print(df.table, type="latex", include.rownames=num, table.placement="!ht", file=dsn, append=ajout)
#    cat("\\vfill\n\\clearpage\n")
    ajout <- TRUE
    ligne <- fin +1
  }
  carp(" dsn: %s", dsn)
}
#
# conversion en table latex
tex_df2table_v1 <- function(df, dsn=FALSE, suffixe="", num=FALSE, nb_lignes=50) {
  library(xtable)
  if ( dsn == FALSE ) {
    curcall <- as.character(deparse(sys.call(-1)))
    curcall <- gsub('\\(.*$', '', curcall)
    if ( suffixe != "" ) {
      suffixe <- sprintf("_%s", suffixe)
    }
    dsn <- sprintf("%s\\%s%s.tex", texDir, curcall, suffixe)
  }
  carp(" dsn: %s", dsn)
#  View(df)
  df <- as.data.frame(df)
#  print(head(df))
  flush.console()
  Sys.sleep(1)
  TEX <- file(dsn, encoding="UTF-8")
  sink(TEX)
  ajout <- FALSE
  ligne <- 1
#  nb_lignes <- 50
  row.names(df) <- 1:nrow(df)
  while ( ligne < nrow(df) ) {
    fin <- ligne + nb_lignes - 1
    fin <- min(fin, nrow(df))
    dfl <- df[ligne:fin,]
#    Log(sprintf("atlas_combine_stat() ligne: %d nrow: %d", ligne, nrow(dfl)))
    df.table <- xtable(dfl, digits=0)
#    align(df.table, "rlrrrrr")
    print(df.table, type="latex", include.rownames=num, table.placement="!ht")
#    cat("\\vfill\n\\clearpage\n")
    ajout <- TRUE
    ligne <- fin +1
  }
  sink()
#  print(sprintf("tex_df2table() dsn: %s", dsn))
}
escapeLatexSpecials <- function(x) {
#  x <- gsub("\\", "$\\backslash$", x, fixed = T)
  x <- gsub("#", "\\\\#", x)
#  x <- gsub("$", "\\\\$", x)
  x <- gsub("%", "\\\\%", x)
#  x <- gsub("&", "***", x)
  x <- gsub("&", "\\\\&", x)
  x <- gsub("~", "\\\\~", x)
  x <- gsub("_", "\\\\_", x)
#  x <- gsub("^", "\\\\^", x)
  x <- gsub("\\{", "\\\\{", x)
  x <- gsub("\\}", "\\\\}", x)
  x <- gsub(">", "$>$", x)
  x <- gsub("<", "$<$", x)
  return(x)
}

tex_df2tpl <- function(df, i, tpl) {
  Encoding(tpl) <- 'UTF-8'
  for ( v in colnames(df) ) {
    re <- paste0("\\{\\{", v, "\\}\\}")
    val <- as.character(df[i, v])
    val <- iconv(val, to='UTF-8')
    val <- escapeLatexSpecials(val)
#    Encoding(val) <- 'UTF-8'
    tpl <-  gsub(re, val, tpl, perl=TRUE)
    Encoding(tpl) <- 'UTF-8'
  }
  return(invisible(tpl))
}