# <!-- coding: utf-8 -->
#
# fonctions génériques
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
Log <- function(msg) {
  print(msg)
  flush.console()
}
#
# comme en perl
carp <- function(...) {
  curcall <- as.character(deparse(sys.call(-1)))
  arguments <- as.list(match.call(expand.dots=FALSE))[-1]
  arguments <- arguments$...
  msg <- ""
  if (length(arguments) >= 1 ) {
    msg <- sprintf(...)
  }
  print(sprintf("%s %s", curcall, msg))
  flush.console()
}
#
# fonctions "système de fichiers"
# ===============================
# options("encoding" = "UTF-8")
# getOption("encoding")
# options("encoding" = "native.enc")
# Encoding(t2[,"a"]) <- "UTF-16"
writeLignes_v1 <- function(fic, lignes, encoding="native.enc") {
  file.create(fic, encoding="UTF-8")
  fileConn <- file(fic)
  if ( encoding == "native.enc" ) {
    msg <- iconv(lignes, to="UTF-8")
  } else {
    msg <- lignes
  }
  writeLines(msg, fileConn, useBytes=TRUE)
  close(fileConn)
  carp(" fic: %s", fic)
}
writeLignes <- function(fic, lignes, encoding="native.enc") {
  TEX <- file(fic, encoding="UTF-8")
  write(lignes, file = TEX, append = FALSE)
  carp(" fic: %s", fic)
}
#
# fonctions pour les fichiers compressés
#
# fonction générique d'extraction
extrait <- function(target_file, target_dir, overwrite=TRUE) {
  carp("target_file: %s", target_file)
  carp("target_dir: %s", target_dir)
  if ( ! file.exists(target_dir) ) {
    dir.create(target_dir, showWarnings = FALSE, recursive = TRUE)
  }
  ext <- sub('^.*\\.','', target_file, perl=TRUE)
  carp("ext:%s", ext)
  switch(ext,
    '7z' = {
      un7z(target_file, exdir=target_dir, overwrite=TRUE)
    },
    'zip' = {
      unzip(target_file, exdir=target_dir, overwrite=TRUE)
    },
    'tar' = {
      untar(target_file, exdir=target_dir)
    }
  )
}
# C:/Program Files/7-Zip/7z.exe
un7z <- function(target_file, exdir, overwrite=TRUE) {
  cmd <-sprintf('7z e -o %s %s', exdir, target_file)
  system(cmd)
}
#
# fonction pour dataframe
# ========================
#
# sauvegarde d'un dataframe en csv
df_ecrire <- function(df,f) {
  Log(sprintf("df_ecrire() f:%s",f))
  if ( ('point' %in% colnames(df)) ) {
    dfTmp <- subset(df, select = -(point) )
    Log(sprintf("df_ecrire() -point"))
  } else {
#    Log(sprintf("df_ecrire() #point"))
    dfTmp <- df
  }
  write.table(dfTmp, file = f, sep = ";", row.names=FALSE, col.names = TRUE,qmethod = "double", quote = FALSE, fileEncoding="utf8")
}
#
# version avec java
df_ecrirex_java <- function(df, dsn=FALSE, suffixe="") {
  library(xlsx)
  df <- as.data.frame(df)
  if ( dsn == FALSE ) {
    curcall <- as.character(deparse(sys.call(-1)))
    curcall <- gsub('\\(.*$', '', curcall)
    if ( suffixe != "" ) {
      suffixe <- sprintf("_%s", suffixe)
    }
    dsn <- sprintf("%s/%s%s.csv", texDir, curcall, suffixe)
  }
  if ( ('_point' %in% colnames(df)) ) {
    dfTmp <- subset(df, select = -(point) )
    Log(sprintf("df_ecrirex() -point"))
  } else {
#    Log(sprintf("df_ecrirex() #point"))
    dfTmp <- df
  }
  write.table(dfTmp, file = dsn, sep = ";", row.names=FALSE, col.names = TRUE, qmethod = "double", quote = FALSE, fileEncoding="utf8")
  dsn <- gsub("csv$", "xlsx", dsn)
  write.xlsx(dfTmp, dsn, row.names=FALSE)
  carp("df_ecrirex() dsn:%s", dsn)
}
df_ecrirex <- function(df, dsn=FALSE, suffixe="") {
  library(writexl)
  df <- as.data.frame(df)
  if ( dsn == FALSE ) {
    curcall <- as.character(deparse(sys.call(-1)))
    curcall <- gsub('\\(.*$', '', curcall)
    if ( suffixe != "" ) {
      suffixe <- sprintf("_%s", suffixe)
    }
    dsn <- sprintf("%s/%s%s.csv", texDir, curcall, suffixe)
  }
  if ( ('_point' %in% colnames(df)) ) {
    dfTmp <- subset(df, select = -(point) )
    Log(sprintf("df_ecrirex() -point"))
  } else {
#    Log(sprintf("df_ecrirex() #point"))
    dfTmp <- df
  }
  write.table(dfTmp, file = dsn, sep = ";", row.names=FALSE, col.names = TRUE, qmethod = "double", quote = FALSE, fileEncoding="utf8")
  dsn <- gsub("csv$", "xlsx", dsn)
  write_xlsx(dfTmp, dsn)
  carp("df_ecrirex() dsn:%s", dsn)
}
df_lire <- function(f) {
  Log(sprintf("df_lire f:%s",f))
  df <- read.table(f, header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="", encoding="UTF-8")
  return(df)
}
df_lirex <- function(dsn) {
  library(xlsx)
  Log(sprintf("df_lirex f:%s", dsn))
  df <- read.xlsx(dsn, 1, header=TRUE, colClasses=NA)
  return(df)
}
df_lirexu <- function(dsn) {
  library(xlsx)
  Log(sprintf("df_lirexu f:%s", dsn))
  df <- read.xlsx(dsn, 1, header=TRUE, colClasses=NA, encoding="UTF-8", stringsAsFactors=FALSE)
  return(df)
}
zip_dl <- function(les_url, base="") {
  for(i in 1:nrow(les_url) ) {
    url <- les_url$url[i]
    zip <-sub(".*/", "", url)
    zip <- les_url$libelle[i]
    url <- sprintf("%s%s", base, url)
    Log(sprintf("zip_dl() url:%s %s %s", url, zip, les_url$libelle[i]))
    file <- sprintf("%s/%s", dl_dir, zip)
    if( ! file.exists(file) ) {
      Log(sprintf("zip_dl() dl file:%s", file ))
      download.file(url, file, quiet = FALSE, mode = "wb")
    }
    if( ! file.exists(file) ) {
      Log(sprintf("zip_dl() absent : file:%s", file))
	    stop()
    }
    zip_dir  <-sub("\\..*$", "", zip)
    zip_dir <- sprintf("%s/%s", dl_dir, zip_dir)
    if( ! file.exists(zip_dir) ) {
      Log(sprintf("zip_dl() absent : zip_dir:%s", zip_dir))
      unzip(file, overwrite = TRUE,junkpaths = TRUE, exdir = zip_dir, unzip = "internal", setTimes = TRUE)
    } else {
      Log(sprintf("zip_dl() present : zip_dir:%s", zip_dir))
    }
    files <- list.files(zip_dir, pattern = "\\.shp$", full.names = TRUE, ignore.case = TRUE)
    if ( length(files) != 1) {
      stop("zip_dl() .shp")
      next;
    }
    dsn <- files[1]
    layers <- ogrListLayers(dsn)
    Log(sprintf("zip_dl() layers:%s", layers))
#    Log(sprintf("inpn() info:%s", OGRSpatialRef(dsn,layers)))
    sp <- readOGR(dsn,layers)
    print(summary(sp))
  }
}
especes_camel <- function(df) {
  df$espece <- gsub(",.*", "", df$TAXO_VERNACUL)
  df$espece_c <- camel2(df$espece)
  df <- df[with(df, order(espece_c)), ]
}
# http://stackoverflow.com/questions/11672050/how-to-convert-not-camel-case-to-camelcase-in-r
camel2 <- function(x) {
  x <- iconv(x, to='ASCII//TRANSLIT')
  gsub("(^|[^[:alnum:]])([[:alnum:]])", "\\U\\2", x, perl = TRUE)
}
camel3 <- function(x) {
  x <- iconv(x, to='ASCII//TRANSLIT')
  x <- gsub("[^[:alnum:]]", " ", x, perl = TRUE)
  x <- tolower(x)
}
# http://stackoverflow.com/questions/11672050/how-to-convert-not-camel-case-to-camelcase-in-r
camel4 <- function(x) {
  x <- iconv(x, from='UTF-8', to='ASCII//TRANSLIT')
  x <- gsub("\\s+", " ", x, perl = TRUE)
  x <- gsub("Oe", "O", x, perl = TRUE)
  x <- gsub("oe", "o", x, perl = TRUE)
  gsub("(^|[^[:alnum:]])([[:alnum:]])", "\\U\\2", x, perl = TRUE)
}
camel5 <- function(x) {
  x <- iconv(x, from='UTF-8', to='ASCII//TRANSLIT')
  x <- gsub("\\-", " ", x, perl = TRUE)
  x <- gsub("\\(", " ", x, perl = TRUE)
  x <- gsub("\\)", " ", x, perl = TRUE)
  x <- gsub("\\s+", " ", x, perl = TRUE)
  x <- gsub("\\s$", "", x, perl = TRUE)
  gsub("(^|[^[:alnum:]])([[:alnum:]])", "\\U\\2", x, perl = TRUE)
}

# http://www.dataanalytics.org.uk/Data%20Analysis/TipsAndTricks/TTR-20150531.htm
## Transparent colors
## Mark Gardener 2015
## www.dataanalytics.org.uk
misc_col <- function(color, percent = 100, name = NULL) {
#	  color = color name
#	percent = % transparency
#	   name = an optional name for the color

## Get RGB values for named color
  rgb.val <- col2rgb(color)
#  print(sprintf("misc_col() color:%s %d %d %d", color, rgb.val[1], rgb.val[2], rgb.val[3]))
## Make new color using input color as base and alpha set by transparency
  col <- rgb(rgb.val[1], rgb.val[2], rgb.val[3],
               max = 255,
#               alpha = (100-percent)*255/100,
               alpha = percent,
               names = name)
#  print(t.col)
#  t.col = rgb(255,0,0, max=255, alpha = 100)
  return(col)
}
# http://www.dataanalytics.org.uk/Data%20Analysis/TipsAndTricks/TTR-20150531.htm
## Transparent colors
## Mark Gardener 2015
## www.dataanalytics.org.uk
t_col <- function(color, percent = 50, name = NULL) {

#	  color = color name
#	percent = % transparency
#	   name = an optional name for the color

## Get RGB values for named color
  rgb.val <- col2rgb(color)
  print(sprintf("t_col() color:%s %d %d %d", color, rgb.val[1], rgb.val[2], rgb.val[3]))
## Make new color using input color as base and alpha set by transparency
  t.col <- rgb(rgb.val[1], rgb.val[2], rgb.val[3],
               max = 255,
#               alpha = (100-percent)*255/100,
               alpha = 60,
               names = name)
#  print(t.col)
#  t.col = rgb(255,0,0, max=255, alpha = 100)
  return(t.col)
}
add.alpha <- function(col, alpha=0.2){
  apply(sapply(col, col2rgb)/255, 2, function(x) rgb(x[1], x[2], x[3], alpha=alpha))
}
plotImg <- function(img, alpha=255, maxpixels=500000) {
  carp()
  dev.new( width = img@ncols, height = img@nrows, units = "px", pointsize = 12)
#  show(img)
  if ( is(img, 'RasterBrick') ) {
    plotRGB(img, axes=FALSE, add=FALSE, maxpixels=maxpixels, alpha=alpha)
  }
  if ( is(img, 'RasterLayer') ) {
    plot(img, axes=FALSE, add=FALSE, maxpixels=maxpixels, alpha=alpha)
  }
  setSizes()
}
makeTransparent = function(..., alpha=0.5) {
  if(alpha<0 | alpha>1) stop("alpha must be between 0 and 1")
  alpha = floor(255*alpha)
  newColor = col2rgb(col=unlist(list(...)), alpha=FALSE)
  .makeTransparent = function(col, alpha) {
    rgb(red=col[1], green=col[2], blue=col[3], alpha=alpha, maxColorValue=255)
  }
  newColor = apply(newColor, 2, .makeTransparent, alpha=alpha)
  return(newColor)
}
#
# détermination de la taille des polices, lignes
setSizes <- function() {
  x1 <- par()$usr[1]
  x2 <- par()$usr[2]
  y1 <- par()$usr[3]
  y2 <- par()$usr[4]
  xextent <- x2 - x1
  yextent <- y2 - y1
#  lgd <- legend(x = mean(c(xmin,xmax)), y =  mean(c(ymin,ymax)), c("Your data name"), pch = c(20), col = c("black"), plot = F)
# Add legend in the lower right corner:
#  legend(x = xmax - lgd$rect$w, y =  ymin + lgd$rect$h, c("Your data name"), pch = c(20), col = c("black"), plot = T)
  cexH1 <<- round(xextent / 300, 1)
#  print(sprintf("setSizes() xextent:%s cexH1:%s", xextent, cexH1))
}
#
# sauvegarde en fichier pdf du graphique ggplot
ggpdf <- function(suffixe="",  width=7, height=7) {
  curcall <- as.character(deparse(sys.call(-1)))
  curcall <- gsub('\\(.*$', '', curcall)
  if ( suffixe != "" ) {
    suffixe <- sprintf("_%s", suffixe)
  }
  dsn <- sprintf("%s/%s%s.pdf", texDir, curcall, suffixe)
  print(sprintf("%s() dsn : %s", curcall, dsn))
  ggsave(dsn, width=width, height=height)
}
ggpdf2 <- function(p, suffixe="", width=7, height=7) {
#  library(cowplot)
  curcall <- as.character(deparse(sys.call(-1)))
  curcall <- gsub('\\(.*$', '', curcall)
  if ( suffixe != "" ) {
    suffixe <- sprintf("_%s", suffixe)
  }
  dsn <- sprintf("%s/%s%s.pdf", texDir, curcall, suffixe)
  print(sprintf("%s() dsn : %s", curcall, dsn))
  ggsave(dsn, p, width=width, height=height)
}
#
# sauvegarde en fichier pdf d'un graphique
dev2pdf <- function(suffixe="") {
  curcall <- as.character(deparse(sys.call(-1)))
  curcall <- gsub('\\(.*$', '', curcall)
  if ( suffixe != "" ) {
    suffixe <- sprintf("_%s", suffixe)
  }
  dsn <- sprintf("%s/%s%s.pdf", texDir, curcall, suffixe)
  dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
  invisible(dev.off())
  carp(" dsn: %s", dsn)
}
#
# sauvegarde en fichier pdf d'un graphique
tex2fic_v1 <- function(tex, suffixe="") {
  curcall <- as.character(deparse(sys.call(-1)))
  curcall <- gsub('\\(.*$', '', curcall)
  if ( suffixe != "" ) {
    suffixe <- sprintf("_%s", suffixe)
  }
  dsn <- sprintf("%s\\%s%s.tex", texDir, curcall, suffixe)
#  Encoding(tex) <- "UTF-8"
  tex <- paste(tex, collapse="\n")
  tex <- iconv(tex, to='UTF-8')
  writeBin(tex, dsn)
  carp(" dsn: %s", dsn)
}
#
# sauvegarde en fichier pdf d'un graphique
tex2fic <- function(tex, suffixe="") {
  curcall <- as.character(deparse(sys.call(-1)))
  curcall <- gsub('\\(.*$', '', curcall)
  if ( suffixe != "" ) {
    suffixe <- sprintf("_%s", suffixe)
  }
  dsn <- sprintf("%s/%s%s.tex", texDir, curcall, suffixe)
#  Encoding(tex) <- "UTF-8"
#  tex <- iconv(tex, to='UTF-8')
  write(tex, dsn)
  carp(" dsn: %s", dsn)
}